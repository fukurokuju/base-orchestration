#!/usr/bin/env bash
set -eou pipefail

ROOT_PATH=${ROOT_PATH:-/mnt/pve/storage/template/iso}
CLOUD_IMAGE_URL=${CLOUD_IMAGE_URL:-https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img}
CLOUD_IMAGE_NAME=${CLOUD_IMAGE_NAME:-focal-server-cloudimg-amd64.img}
CLOUD_IMAGE_CHECKSUMS_URL=${CLOUD_IMAGE_CHECKSUMS_URL:-https://cloud-images.ubuntu.com/focal/current/SHA256SUMS}

if virt-customize --help >/dev/null; then echo libguestfs-tools are installed; else echo -e "libguestfs-tools not installed\n... exiting without changes" && exit 1; fi
cd "$ROOT_PATH"
if wget -O "$CLOUD_IMAGE_NAME" "$CLOUD_IMAGE_URL" >/dev/null; then echo cloudinit image downloaded; else echo -e cloudinit image not downloaded; fi
virt-customize -a "$CLOUD_IMAGE_NAME" --install qemu-guest-agent
virt-customize -a "$CLOUD_IMAGE_NAME" --install vim
virt-customize -a "$CLOUD_IMAGE_NAME" --install git
virt-customize -a "$CLOUD_IMAGE_NAME" --install curl
virt-customize -a "$CLOUD_IMAGE_NAME" --install sudo
virt-customize -a "$CLOUD_IMAGE_NAME" --install fish

qm destroy 1005 | exit 0
qm create 1005 --name "ubuntu-2004-cloudinit-template" --memory 2048 --cores 2 --net0 virtio,bridge=vmbr0
qm importdisk 1005 "$CLOUD_IMAGE_NAME" storage
qm set 1005 --scsihw virtio-scsi-pci --scsi0 storage:1005/vm-1005-disk-0.raw
qm set 1005 --boot c --bootdisk scsi0
qm set 1005 --ide2 storage:cloudinit
qm set 1005 --serial0 socket --vga serial0
qm set 1005 --agent 1
qm template 1005
