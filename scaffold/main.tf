terraform {
  backend "s3" {
    bucket = "fukurokuju-tf"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }

  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.5"
    }
  }
}

provider "proxmox" {
  pm_timeout      = "600"
  pm_tls_insecure = true
  pm_log_levels = {
    _default = "debug"
  }
}


resource "proxmox_lxc" "outer_heaven" {
  vmid        = 1119
  target_node = "ramiel"
  hostname    = "outerheaven"
  ostemplate  = "storage:vztmpl/debian-11-standard_11.0-1_amd64.tar.gz"
  swap        = "14336"
  memory      = "10240"
  start       = true
  cores       = 2
  onboot      = true
  rootfs {
    storage = "storage"
    size    = "64G"
  }
  mountpoint {
    key     = "1"
    slot    = 1
    storage = "jelly"
    volume  = "/jelly"
    mp      = "/jelly"
    size    = "5000G"
  }
  mountpoint {
    key     = "2"
    slot    = 2
    storage = "jelly2"
    volume  = "/mnt/pve/storage/shared/jelly"
    mp      = "/mnt/pve/storage/shared/jelly"
    size    = "5000G"
  }
  network {
    name   = "eth0"
    bridge = "vmbr0"
    ip     = "192.168.1.117/24"
    gw     = "192.168.1.1"
    ip6    = "auto"
  }
  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE6DCfrklPMoAh8w2V7TfuE81upjJaXvlCV/A0doE/3s catalin@roboces.dev
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@fukurokuju.dev
  EOT
}


resource "proxmox_vm_qemu" "sahelanthropus" {
  count       = 1
  vmid        = 1118
  name        = "sahelanthropus"
  target_node = var.proxmox_host
  clone       = var.template_name
  agent       = 1
  os_type     = "cloud-init"
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 12288
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  onboot      = true
  disk {
    slot     = 0
    size     = "40140M"
    type     = "scsi"
    storage  = "storage"
    iothread = 1
  }
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }
  lifecycle {
    ignore_changes = [
      network,

    ]
  }
  ipconfig0 = "ip=192.168.1.118/24,gw=192.168.1.1"
  sshkeys   = <<EOF
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@fukurokuju.dev
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII8LvY/0JHgBNnVG9kTQfxmyTPq0pyQRbFOZqFPpIHl6 catalin@roboces.dev
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1aEkUVzYhn+Bs1e8VWjjel1C56Bj1RPoFkt9E9FCdu runner@gitlab
  EOF
}


resource "proxmox_vm_qemu" "excelsus" {
  count       = 1
  vmid        = 1119
  name        = "excelsus"
  target_node = var.proxmox_host
  clone       = var.template_name
  agent       = 1
  os_type     = "cloud-init"
  cores       = 2
  sockets     = 1
  cpu         = "host"
  memory      = 2048
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  onboot      = true
  disk {
    slot     = 0
    size     = "40140M"
    type     = "scsi"
    storage  = "storage"
    iothread = 1
  }
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }
  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  ipconfig0 = "ip=192.168.1.119/24,gw=192.168.1.1"
  sshkeys   = <<EOF
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@fukurokuju.dev
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII8LvY/0JHgBNnVG9kTQfxmyTPq0pyQRbFOZqFPpIHl6 catalin@roboces.dev
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1aEkUVzYhn+Bs1e8VWjjel1C56Bj1RPoFkt9E9FCdu runner@gitlab
  EOF
}
