# intro

fuku's base orchestation docs

## hardware setup

- 2 switches
- 1 router
- 1 NUC
- 1 rpi 3b

## assumptions

- already-installed proxmox
- ssh access to proxmox and k3s machines (even future ones)
- glusterfs server

## flow

1. kaniko builds a docker image with the needed utils:
    - terraform
    - ansible
    - mkdocs
2. terraform builds proxmox vms
3. k3sup deploys a k3s cluster inside proxmox vms and the rpi3b
5. ansible sets up the machines with the needed programs and configs
